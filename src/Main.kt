import kotlin.math.pow
import kotlin.math.sqrt
fun main(){
    val p1=Point(1.0,2.0)
    val p2=Point(3.0,4.0)
    println(p1)
    println(p1==p2)
    println(-p1)
    println(p1.distance(p2))

}

data class Point(private var x:Double, private var y:Double){
    override fun toString():String{
        return "$x, $y"

    }

    override fun equals(other: Any?): Boolean {
        if (other is Point){
            return other.x==x && other.y==y
        }
        return false

    }
    operator fun unaryMinus(): Point {
        return Point(-x, -y)
    }

    fun distance(other: Point): Double {
        return sqrt((x-other.x).pow(2) + (y-other.y).pow(2))

    }



}